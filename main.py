from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from functools import partial
import sys
import os.path
from Books._ScreenTypes.lessonStruct import *
import Books._ScreenTypes.GlobalVars as gloVars
from settings import *

class MainMenuApp(App):
	def build(self):
	    gloVars.sm.add_widget(MainMenuPage(name = 'menu'))
	    gloVars.sm.add_widget(LessonSelectPage(name = 'select'))
	    gloVars.sm.add_widget(HelpMenuPage(name = 'help'))
	    gloVars.sm.add_widget(SettingsPage(name = 'settings'))

	    gloVars.sm.get_screen('select').lessonsAvail()
	    
	    #screenTypes
	    gloVars.sm.add_widget(IntroPage(name = 'intro'))
	    gloVars.sm.add_widget(MultiLettersPage(name = 'multiletters'))
	    gloVars.sm.add_widget(MultiImagesPage(name = 'multiImages'))
	    gloVars.sm.add_widget(ImageFindLettersPage(name = 'imagefindletters'))
	    gloVars.sm.add_widget(MultiUnitPage(name = 'multiUnit'))
	    gloVars.sm.add_widget(WordListPage(name = 'wordList'))
	    gloVars.sm.add_widget(FindImagePage(name = 'findImage'))
	    gloVars.sm.add_widget(WordBlendPage(name = 'wordBlend'))
	    gloVars.sm.current = 'menu'
	    return gloVars.sm


class MainMenuPage(Screen):
	def loadLesson(self,cont):
		if(cont and os.path.isfile("saveFile.txt")):
			file = open("saveFile.txt","r")
			numList = file.read().split(",")
			file.close()
			if(sec.create(numList[0],numList[1],numList[2],numList[3])):
				sec.showScreen()
		else:
			if(sec.create(1,1,1,0)):
				sec.showScreen()
	def playAudio(self,name):
		gloVars.readS = SoundLoader.load('Books\\_Audio\\'+name+'.mp3')
		gloVars.readS.volume = gloVars.volume
		gloVars.readS.play()

class HelpMenuPage(Screen):
	pass

class LessonSelectPage(Screen):
	def pressed(self,bInt,lInt,sInt, instance):
		sec.create(bInt,lInt,sInt,0)
		sec.showScreen()
		
	def lessonsAvail(self):
		sectionList = ObjectProperty()
		bookNum = 1
		lessonNum = 1
		sectionNum = 1
		self.ids.sectionList.clear_widgets()
		## some sudo code that might help can't run at the moment
		## check lessonStruct for section Exists function so you aren't creating overhead by creating the section each time c:
		booksFlag = True
		lessonFlag = True
		sectionFlag = True
		
		while booksFlag:
			while lessonFlag:
				while sectionFlag:
					if(sec.sectionExists(bookNum,lessonNum,sectionNum)):
							self.button = Button(text = str(bookNum)+"."+str(lessonNum)+ "." +str(sectionNum)+":")
							self.ids.sectionList.add_widget(self.button)
							#probs need to import partial, I use it in lesson struct. py
							self.button.bind(on_press = partial(self.pressed, bookNum,lessonNum,sectionNum))
							sectionNum +=1
					else:
						sectionFlag = False
				#this is now in lessons
				lessonNum += 1
				sectionNum = 1
				if  sec.sectionExists(bookNum,lessonNum,sectionNum):
					sectionFlag = True
				else:
					lessonFlag = False
			#this is now in Books
			bookNum +=1
			lessonNum = 1
			sectionNum = 1
			if  sec.sectionExists(bookNum,lessonNum,sectionNum):
				lessonFlag = True
				sectionFlag = True
			else:
				bookFlag = False
				break
				

			
				
				
##				
##		for bookNum in range(1,20):
##			for lessonNum in range(1,20):
##				for sectionNum in range(1,20):
##					if(sec.create(bookNum,lessonNum,sectionNum,0)):
##					   #create buttons here
##					   self.button = Button(text = bookNum+"."+lessonNum+ "." +sectionNum+":")
##					else:
##						print("No")
##					self.ids.sectionList.add_widget(self.button)
##					self.button.bind(on_press = self.pressed)
				
    


if __name__ == '__main__':
	MainMenuApp().run()


