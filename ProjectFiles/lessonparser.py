from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import ObjectProperty
from kivy.core.audio import SoundLoader

def getCommands(lessonName):
	lesson = open(lessonName,"r")
	lessonText = lesson.read()
	commandList = lessonText.split("\n")
	return commandList

commands = getCommands("Lessons\Lesson1.lesson")
correctMessages = list()
incorrectMessages = list()
correctAudio = list()
incorrectAudio = list()

for com in commands:
	words = com.split(";")
	if(words[0] == "CORRECTMESSAGE"):
		for message in words:
			if(message != "CORRECTMESSAGE"):
				correctMessages.append(message)
	elif(words[0] == "CORRECTAUDIO"):
		for file in words:
			if(file != "CORRECTAUDIO"):
				correctAudio.append(file)
	elif(words[0] == "INCORRECTMESSAGE"):
		for message in words:
			if(message != "INCORRECTMESSAGE"):
				incorrectMessages.append(message)
	elif(words[0] == "INCORRECTAUDIO"):
		for file in words:
			if(file != "INCORRECTAUDIO"):
				incorrectAudio.append(file)
	elif(words[0] == "TEXT"):
		pass#Set the current text on screen to words[1]
	elif(words[0] == "IMAGE"):
		pass#Set the current image on display to words[1]
	elif(words[0] == "AUDIO"):
		pass#Play the sound specified by words[1]
	elif(words[0] == "DIALOGUE"):
		pass#Like text and audio together
	elif(words[0] == "BACKGROUND"):
		pass#Sets background image
	elif(words[0] == "MULTIPLECHOICE"):
		pass#Open the multiple choice screen, with words[1] as the correct answer, and 2,3,4 as incorrect
	elif(words[0] == "VOICEQUESTION"):
		pass#Check if voice is enabled
		#If it is, open the voice question screen and compare voice recorded to words[1]
	else:
		pass
		
class StartPage(Screen):

	def changeText(value):
		pass#shrugging intensifies