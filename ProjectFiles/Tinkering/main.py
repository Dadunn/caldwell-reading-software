from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import StringProperty
import sys

class MainMenuPage(Screen):
	global textList #make the text list usable in nextText()
	textList = ["Test","Change successful","Awesome"] #initialize the text list
	text = textList.pop(0) #Pop the first value in the list, making it the value of text
	def nextText(self):
		try:
			text = textList.pop(0) #same as line 11, but does not seem to change the value on-screen. scoping errors?
			print(text) #prints the value of text to the command line
		except IndexError:
			sys.exit() #Exit when the list is empty and we try to get a new message

class MainMenuApp(App):
	def build(self):
		sm = ScreenManager()
		sm.add_widget(MainMenuPage(name = 'menu'))
		sm.current = 'menu'
		return sm
	

if __name__ == '__main__':
	MainMenuApp().run()