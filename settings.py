from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import ObjectProperty
from kivy.core.audio import SoundLoader
import Books._ScreenTypes.GlobalVars as gloVars
import os

class SettingsPage(Screen):
	examText = "Settings"
	
	def changeBgVolume(self, value):
		gloVars.sound.volume = value
		
	def changeSpeakerVolume(self, value):
		gloVars.volume = value
		
		
