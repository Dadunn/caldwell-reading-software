from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.core.audio import SoundLoader

#global screenmanager for all classes and everything to use.
sm = ScreenManager()
volume = 0.7

sound = SoundLoader.load('Books\\_Audio\\VibrantDreams.wav')
sound.play()
sound.loop = True
sound.volume = .3

readS = SoundLoader.load('Books\\_Audio\\CRS.mp3')
readS.play()
readS.loop = False
readS.volume = .7

