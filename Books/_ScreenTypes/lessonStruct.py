from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.label import Label
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.image import Image
from kivy.core.audio import SoundLoader
from functools import partial
from random import randrange
from random import randint
import sys, os
		
import pyaudio
import wave


import Books._ScreenTypes.GlobalVars as GlobalVars


class Section(object):
	filePointer = None
	bookNum = 0
	lessonNum = 0
	sectionNum = 0
	pageNum = 0
	currentScreen = None

	def readAudioArray(self, array):
		if(len(array) > 0):
			readSound = SoundLoader.load('Books\\_Audio\\'+array[0]+'.mp3')
			readSound.volume = GlobalVars.volume
			readSound.bind(on_stop=partial(self.nextInAudioArray,array))
			readSound.play()
			
	def nextInAudioArray(self, array, instance):
		array.pop(0)
		self.readAudioArray(array)
			
		
		
	def loadNextFile(self):
		self.filePointer.close()
		self.sectionNum += 1
		scriptDir = sys.path[0]
		#checkPath = scriptDir+'\\Books\\Book'+str(self.bookNum)+'\\Lesson'+str(self.lessonNum)+'\\Section'+str(self.sectionNum)+'.txt'
		if(self.create(self.bookNum,self.lessonNum,self.sectionNum,0)):
			return
		else:
			self.sectionNum = 1
			self.lessonNum += 1

		#checkPath = scriptDir+'\\Books\\Book'+str(self.bookNum)+'\\Lesson'+str(self.lessonNum)+'\\Section'+str(self.sectionNum)+'.txt'
		if(self.create(self.bookNum,self.lessonNum,self.sectionNum,0)):
			return
		else:
			self.lessonNum = 1
			self.bookNum += 1

		#checkPath = scriptDir+'\\Books\\Book'+str(self.bookNum)+'\\Lesson'+str(self.lessonNum)+'\\Section'+str(self.sectionNum)+'.txt'
		if(self.create(self.bookNum,self.lessonNum,self.sectionNum,0)):
			return
		else:
			self.currentScreen = 'menu'
			
	def recordVoice(self):
		CHUNK = 1024
		FORMAT = pyaudio.paInt16
		CHANNELS = 2
		RATE = 44100
		RECORD_SECONDS = 3
		WAVE_OUTPUT_FILENAME = "output.wav"
		
		p = pyaudio.PyAudio()
		
		stream = p.open(format=FORMAT,
			channels=CHANNELS,
			rate=RATE,
			input=True,
			frames_per_buffer=CHUNK)
		GlobalVars.sound.stop()
		
		frames = []

		for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
		    data = stream.read(CHUNK)
		    frames.append(data)
		
		GlobalVars.sound.play()
		stream.stop_stream()
		stream.close()
		p.terminate()

		wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
		wf.setnchannels(CHANNELS)
		wf.setsampwidth(p.get_sample_size(FORMAT))
		wf.setframerate(RATE)
		wf.writeframes(b''.join(frames))
		wf.close()
		
		
	def playAudio(self):
		out = SoundLoader.load('output.wav')
		out.loop = False
		out.play()
			
	def readHeader(self):
		bookArray = self.filePointer.readline().split(";")
		self.bookNum = int(bookArray[1])
		lessonArray = self.filePointer.readline().split(";")
		self.lessonNum = int(lessonArray[1])
		sectionArray = self.filePointer.readline().split(";")
		self.sectionNum = int(sectionArray[1])

		self.filePointer.readline()
		
	
	def ReadNextInFile(self):
		sec.deleteSave()
		sec.save()
		self.pageNum += 1
		screenArray = []
		tempString = ""
		flag = True
		while(flag):
			tempString = self.filePointer.readline()
			if(tempString == '\n' or tempString == '\r\n' or tempString == '' ):
				flag = False
			else:
				screenArray.append(tempString)
		return screenArray
		
	def choseScreen(self,screenArray):
		screenTypeHeader = screenArray[0].split(";")
		if(screenTypeHeader[0] == "END"):
			self.loadNextFile()
		elif(screenTypeHeader[0] == "INTRO"):
			GlobalVars.sm.get_screen('intro').initValues(screenArray)
			self.currentScreen = 'intro'
		elif(screenTypeHeader[0] == "MULTILETTERS"):
			GlobalVars.sm.get_screen('multiletters').initValue(screenArray)
			self.currentScreen = 'multiletters'
		elif(screenTypeHeader[0] == "MULTIIMAGES"):
			GlobalVars.sm.get_screen('multiImages').initValues(screenArray)
			self.currentScreen = 'multiImages'
		elif(screenTypeHeader[0] == "IMAGEFINDLETTERS"):
			GlobalVars.sm.get_screen('imagefindletters').initValue(screenArray)
			self.currentScreen = 'imagefindletters'
		elif(screenTypeHeader[0] == "MULTIUNIT"):
			GlobalVars.sm.get_screen('multiUnit').initValue(screenArray)
			self.currentScreen = 'multiUnit'
		elif(screenTypeHeader[0] == "WORDLIST"):
			GlobalVars.sm.get_screen('wordList').initValue(screenArray)
			self.currentScreen = 'wordList'
		elif(screenTypeHeader[0] == "FINDIMAGE"):
			GlobalVars.sm.get_screen('findImage').initValue(screenArray)
			self.currentScreen = 'findImage'
		elif(screenTypeHeader[0] == "WORDBLEND"):
			GlobalVars.sm.get_screen('wordBlend').initValues(screenArray)
		else:
			self.NextScreen()
			
	def deleteSave(self):
		try:
			os.remove("saveFile.txt")
		except FileNotFoundError:
			pass
			
	def save(self):
		self.deleteSave()
		file = open("saveFile.txt","w")
		file.write(str(self.bookNum)+","+str(self.lessonNum)+","+str(self.sectionNum)+","+str(self.pageNum))
		file.close()
	
	def showScreen(self):
		GlobalVars.sm.current = self.currentScreen

	def NextScreen(self):
		sectionArray = self.ReadNextInFile()
		self.choseScreen(sectionArray)
		self.showScreen()
		
	def sectionExists(self,bInt,lInt,sInt):
		fileName = "Books\Book" + str(bInt) + "\Lesson" + str(lInt) + "\Section" + str(sInt) + ".txt"
		try:
			fp = open(fileName, 'r', 1)
		except FileNotFoundError:
			return False
		fp.close
		return True
		
	def create(self,bInt,lInt,sInt,pInt):
		self.bookNum = int(bInt)
		self.lessonNum = int(lInt)
		self.sectionNum = int(sInt)
		self.pageNum = 0
		fileName = "Books\Book" + str(self.bookNum) + "\Lesson" + str(self.lessonNum) + "\Section" + str(self.sectionNum) + ".txt"
		try:
			self.filePointer = open(fileName, 'r', 1)
		except FileNotFoundError:
			return False
		self.readHeader()
		for k in range(0,int(pInt)+1):
			screenArray = self.ReadNextInFile()
		self.choseScreen(screenArray)
		return True
		
sec = Section()
	#example screen
class IntroPage(Screen):
	upper = True
	letterText = ObjectProperty()
	readText = ObjectProperty()
	unit = "temp"

	def playReadText(self):
		array =['thisLetterSays', self.unit, 'moveTheCursorTo', self.unit, 'say', self.unit, 'good1', 'thatSays', self.unit,'say', self.unit]
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	#variables specific to intropage
	def initValues(self, screenArray):
		self.unit = screenArray[0].split(";")[1].lower()
		self.playReadText()

		if(self.upper == True):
			self.ids.letterText.text = self.unit.upper()
		else:
			self.ids.letterText.text = self.unit.lower()

		if(len(self.unit) > 1):
			self.ids.readText.text = "This unit says \""+self.unit+"\". Move the cursor to \""+self.unit+"\" and say \""+self.unit+"\".Good! That says \""+self.unit+"\"; say \""+self.unit+"\"."
		else:
			self.ids.readText.text = "This letter says \""+self.unit+self.unit+"\". Move the cursor to \""+self.unit+self.unit+"\" and say \""+self.unit+self.unit+"\".Good! That says \""+self.unit+self.unit+"\"; say \""+self.unit+self.unit+"\"."
			
	def goToNextScreen(self):
		if(self.upper == True):
			lowerText = []
			lowerText.append("x;"+self.unit+";")
			self.upper = False
			self.initValues(lowerText)
		else:
			self.upper = True
			sec.NextScreen()

class MultiLettersPage(Screen):
	instruct = ObjectProperty()
	unit = "temp"
	right = 0
	score = 0

	def playReadText(self):
		array =['findAllTheLettersThatSay', self.unit, 'say', self.unit,'AsYouFindThem']
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButThere', self.unit, 'lettersOnTheScreen']
		sec.readAudioArray(array)
		
	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	def rightAnswer(self, instance):
		
		array = ['good1']
		sec.readAudioArray(array)
		self.score += 1
		instance.unbind(on_press=self.rightAnswer)        

	def initValue(self, screenArray):
		self.right = 0
		self.score = 0
		self.ids.letter.clear_widgets()
		self.unit = screenArray[0].split(";")[1].lower()
		self.playReadText()
		self.ids.instruct.text = "Find all of the letters that say \""+self.unit+" \" and say \""+self.unit+" \" as you find them"
		self.button = Button(text = self.unit)
		self.ids.letter.add_widget(self.button)
		self.right += 1
		self.button.bind(on_press= self.rightAnswer)
		textVar = '' 
		for k in range(1,6):
			numV = randint(1,8)
			if numV <= 6:
				randomLetter = chr(randrange(97, 122))
				self.button = Button(text = randomLetter)
				textVar = randomLetter
			else:
				if numV == 7:
					self.button = Button(text = self.unit.lower())
					textVar = self.unit.lower()
				else:
					self.button = Button(text = self.unit.upper())
					textVar = self.unit.upper()
			if textVar == self.unit.lower():
				self.right += 1
				self.button.bind(on_press= self.rightAnswer)
			elif textVar == self.unit.upper():
				self.right += 1
				self.button.bind(on_press= self.rightAnswer)
			self.ids.letter.add_widget(self.button)
					    
			
	def goToNext(self):
		if(self.score != self.right):
			self.playHelpText()
			self.ids.instruct.text = "Good job so far, but there are still \""+self.unit+"\" letters on the screen."
		else:
			sec.NextScreen()

					
class ImageFindLettersPage(Screen):
	fileName = ObjectProperty()
	sentence = ObjectProperty()
	unit = "temp"
	right = 0
	score = 0


	def playReadText(self):
		array =['fineEachwordInTheSentenceBelow', self.unit,'sound', 'say', self.unit,'AsYouFindThem']
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButThere', self.unit, 'lettersOnTheScreen']
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()
	
	def rightAnswer(self, instance):
		
		array = ['good1']
		sec.readAudioArray(array)
		self.score += 1
		instance.unbind(on_press=self.rightAnswer)
	
	def initValue(self, screenArray):
		self.right = 0
		self.score = 0
		self.ids.iflGrid.clear_widgets()
		self.ids.iflImageBox.clear_widgets()
		commandArray = []
		for entry in screenArray:
			for item in entry.split(";"):
				if(item != "\n"):
					commandArray.append(item)
		self.unit = commandArray[1].lower()
		self.playReadText()
		commandArray.pop(0)
		commandArray.pop(0)
		
		while(len(commandArray) > 0):
			if(commandArray[0] == "NAME"):
				# self.name = commandArray[1] this doesn't do what you think it does
				self.fileName = commandArray[1]
			elif(commandArray[0] == "IMAGETEXT"):
				self.sentence = commandArray[1]
			try:
				commandArray.pop(0)
				commandArray.pop(0)
			except IndexError:
				pass

		self.ids.iflText.text = "Find each word in the sentence below that contains the '" + self.unit.lower() +"' sound and say '" +self.unit.lower() +"' as you find them!"
		imagePath = "Books\\_Images\\"+self.fileName+".png"
		self.button = Image(source=imagePath,allow_stretch=True,keep_ratio=True)
		self.ids.iflImageBox.add_widget(self.button)
		
		for word in self.sentence.split(" "):
			self.button = Button(text=word)
			self.ids.iflGrid.add_widget(self.button)
			if(self.unit.lower() in word.lower()):
				self.right += 1
				self.button.bind(on_press= self.rightAnswer)
	
	def goToNextScreen(self):
		if(self.score == self.right):
			sec.NextScreen()
		else:
			self.playHelpText()
		
class MultiImagesPage(Screen):
	readTextMI = ObjectProperty()
	unit = "temp"
	rightAnswers = 0
	currentScore = 0

	def playReadText(self):
		array =['findThePictures', self.unit,'sound', 'inThem', 'pressThebuttonUnderThePicture']
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButPictures']
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	def correctAnswer(self, instance):
		
		array = ['good1']
		sec.readAudioArray(array)
		self.currentScore += 1
		instance.unbind(on_press=self.correctAnswer)

	def playSound(self, soundPath, instance):
		sound = SoundLoader.load(soundPath)
		sound.volume = .5
		sound.loop = False
		sound.play()
		
	def initValues(self, screenArray):
		self.rightAnswers = 0
		self.currentScore = 0
		self.ids.imageButtons.clear_widgets()
		self.ids.imageSounds.clear_widgets()
		self.unit = screenArray[0].split(";")[1].lower()
		self.playReadText()
		
		if(len(self.unit) > 1):
			self.ids.readTextMI.text = "Find the pictures that have the \""+self.unit+"\"sound in them. Press the button under the picture and I'll tell you what it is a picture of."
		else:
			self.ids.readTextMI.text = "Find the pictures that have the \""+self.unit+self.unit+"\"sound in them. Press the button under the picture and I'll tell you what it is a picture of."

		scriptDir = sys.path[0]
		genericPathImage = 'Books\\_Images\\'
		genericPathAudio = 'Books\\_Audio\\'

		#add buttons as you read them in from the array to the grid
		screenArray.pop(0)
		for i in screenArray:
			filesName = i.split(";")[1]
			#Image buttons
			imagePath = genericPathImage+filesName+'.png'
			
			self.button = Button(background_normal=imagePath)
			if self.unit in filesName:
				self.rightAnswers += 1
				self.button.bind(on_press=self.correctAnswer)
			self.ids.imageButtons.add_widget(self.button)
			#Sound buttons
			
			soundPath = genericPathAudio+filesName+'.mp3'

			self.soundButton = Button()
			#partial(a_function,arg1, arg2)
			self.soundButton.bind(on_press=partial(self.playSound, soundPath))
			self.ids.imageSounds.add_widget(self.soundButton)

			
			
			
			
	def goToNextScreen(self):
		if(self.currentScore != self.rightAnswers):
			self.playHelpText()
			self.ids.readTextMI.text = "Good job so far, but there are still pictures with the sound in them."
		else:
			sec.NextScreen()


class MultiUnitPage(Screen):
	prompt = ObjectProperty
	unit = "temp"
	right = 0
	score = 0

	def playReadText(self):
		array =['findAllTheUnits', self.unit]
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButThere',self.unit,'unitsLeft']
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	def rightAnswer(self, instance):
		self.score += 1
		
		array = ['good1']
		sec.readAudioArray(array)
		instance.unbind(on_press=self.rightAnswer)

	def initValue(self,screenArray):
		self.right = 0
		self.score = 0
		self.ids.unitButton.clear_widgets()
		self.unit = screenArray[0].split(";")[1].lower()
		self.playReadText()
		
		if(len(self.unit)> 1):
			self.ids.prompt.text = "Find all the units that say \""+self.unit+"\"."
		scriptDir = sys.path[0]
		genericPath = 'Books\\_TextFiles\\' +screenArray[1].split(";")[1]+'.txt'
		filePath = genericPath
		filePointer = open(filePath, 'r', 1)
		unitString = filePointer.read()
		unitArray = []
		for units in unitString.split():
			unitArray.append(units.strip())
		for units in unitArray:
			self.button = Button(text = units)
			if self.unit in units:
				self.right += 1
				self.button.bind(on_press=self.rightAnswer)
			self.ids.unitButton.add_widget(self.button)

	def goToNextScreen(self):
		if(self.score != self.right):
			self.playHelpText()
			self.ids.prompt.text = "Good job so far, but there are still \""+self.unit+"\"units left on the screen."
		else:
			sec.NextScreen()

class WordListPage(Screen):
	readTextMI = ObjectProperty()
	unit = "temp"
	rightAnswers = 0
	currentScore = 0

	def playReadText(self):
		array =['findTheWordsThatHave', self.unit,'inThem']
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButWords']
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	def correctAnswer(self, instance):
		self.currentScore += 1
		
		array = ['good1']
		sec.readAudioArray(array)
		instance.unbind(on_press=self.correctAnswer)

	def initValue(self, screenArray):
		self.rightAnswers = 0
		self.currentScore = 0
		self.ids.wordButtons.clear_widgets()
		self.unit = screenArray[0].split(";")[1].lower()
		self.playReadText()
		
		if(len(self.unit) > 1):
			self.ids.readTextMI.text = "Find the words that have the \""+self.unit+"\" in them."
		else:
			self.ids.readTextMI.text = "Find the words that have the \""+self.unit+self.unit+"\" in them."
		
		scriptDir = sys.path[0]
		genericPath = 'Books\\_TextFiles\\'+screenArray[1].split(";")[1]+'.txt'
		filePath = genericPath
		filePointer = open(filePath, 'r', 1)
		wordsString = filePointer.read()
		wordArray = []
		for words in wordsString.split():
			wordArray.append(words.strip())

		#add buttons as you read them in from the array to the grid
		for word in wordArray:
			self.button = Button(text = word)
			if self.unit in word:
				self.rightAnswers += 1
				self.button.bind(on_press=self.correctAnswer)
			self.ids.wordButtons.add_widget(self.button)

			
			
			
			
	def goToNextScreen(self):
		if(self.currentScore != self.rightAnswers):
			self.playHelpText()
			self.ids.readTextMI.text = "Good job so far, but there are words with the sound in them."
		else:
			sec.NextScreen()
			
class FindImagePage(Screen):
	readText = ObjectProperty()
	imageText = ObjectProperty()
	rightAnswers = 0
	currentScore = 0

	def playReadText(self):
		array =['textAbove']
		sec.readAudioArray(array)
		
	def playHelpText(self):
		
		array = ['goodJobSoFarButPicturesLeft']
		sec.readAudioArray(array)

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()

	def correctAnswer(self, instance):
		
		array = ['good1']
		sec.readAudioArray(array)
		self.currentScore += 1
		instance.unbind(on_press=self.correctAnswer)
		
	def initValue(self, screenArray):
		self.rightAnswers = 0
		self.currentScore = 0
		self.ids.imageButtons.clear_widgets()
		self.playReadText()
		self.ids.readText.text = "Find the picture or pictures that go with the text above!"

		scriptDir = sys.path[0]
		genericPathImage = 'Books\\_Images\\'

		#add buttons as you read them in from the array to the grid
		screenArray.pop(0)
		for i in screenArray:
			if i.split(";")[0] == "NAME":
				filesName = i.split(";")[1]
				
				#Image buttons
				imagePath = genericPathImage+filesName+'.png'
				
				self.button = Button(background_normal=imagePath)
				try:
					if i.split(";")[2] == "CORRECT":
						self.rightAnswers += 1
						self.button.bind(on_press=self.correctAnswer)
				except IndexError:
					pass
				self.ids.imageButtons.add_widget(self.button)
			else:
			      self.ids.imageText.text = i.split(";")[1] 

			
	def goToNextScreen(self):
		if(self.currentScore != self.rightAnswers):
			self.playHelpText()
			self.ids.readText.text = "Good job so far, but there are still pictures left that are correct!"
		else:
			sec.NextScreen()			
			
class WordBlendPage(Screen):
	wbWord = "temp"
	wbPre = "temp"
	wbPost = "temp"
	right = 0

	def record(self):
		sec.recordVoice()
	def play(self):
		sec.playAudio()
		
	def setWbPreText(instance):
		self.ids.wbPreText.text = instance.text
		
	def setWbPostText(instance):
		self.ids.wbPostText.text = instance.text
	
	def initValues(self,screenArray):
		self.right = 0
		self.ids.wbPreGrid.clear_widgets()
		self.ids.wbPostGrid.clear_widgets()
		self.ids.wbPreText.text = " "
		self.ids.wbPostText.text = " "
		
		commandArray = []
		for entry in screenArray:
			wbList = entry.split(";")
			if(wbList[0] == "WORDBLEND"):
				self.wbWord = wbList[1]
			elif(wbList[0] == "CORRECT"):
				self.wbPre = entry[1]
				self.wbPost = wbList[2]
			elif(wbList[0] == "PREFIX"):
				wbList.pop(0)
				for prefix in wbList:
					self.button = Button(text=prefix)
					self.ids.wbPreGrid.add_widget(self.button)
					self.button.bind(on_press=self.setWbPreText)
			elif(wbList[0] == "SUFFIX"):
				wbList.pop(0)
				for postfix in wbList:
					self.button = Button(text=postfix)
					self.ids.wbPostGrid.add_widget(self.button)
					self.button.bind(on_press=self.setWbPostText)
			else:
				print("Unexpected keyword in section file.\nPlease ensure that your section file contains no errors.")
				print(entry)
					
		self.ids.wbText.text = "Try to make the word that best describes the image to the left using these word parts! And say the word as you make it."
		self.ids.wbImage.background_normal = 'Books\\_Images\\'+self.wbWord+'.png'
	
	def goToNextScreen(self):
		if(wbPre == self.ids.wbPreText.text and wbPost == self.ids.wbPostText.text):
			sec.NextScreen()
		elif(wbPre != self.ids.wbPreText.text and wbPost == self.ids.wbPostText.text):
			self.ids.wbText.text = "You got the second part right! Try the first part again!"
			self.ids.wbPreText.text = " "
		elif(wbPre == self.ids.wbPreText.text and wbPost != self.ids.wbPostText.text):
			self.ids.wbText.text = "You got the first part right! Try the second part again!"
			self.ids.wbPostText.text = " "
		else:
			self.ids.wbText.text = "That's not it! Try to make the word again!"
			self.ids.wbPreText.text = " "
			self.ids.wbPostText.text = " "
